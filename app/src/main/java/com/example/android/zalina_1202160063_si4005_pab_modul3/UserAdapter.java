package com.example.android.zalina_1202160063_si4005_pab_modul3;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    public UserAdapter(ArrayList<Data> daftarUser, Context mContext) {
        this.daftarUser = daftarUser;
        this.mContext = mContext;
    }

    private ArrayList<Data> daftarUser;
    private Context mContext;

    @NonNull

    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(mContext).
                inflate(R.layout.list_user, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int posisi) {
        Data currentUser = daftarUser.get(posisi);
        viewHolder.bindTo(currentUser);
    }

    @Override
    public int getItemCount() {
        return daftarUser.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView namalengkap, pekerjaan;
        private ImageView fotoprofil;
        private int kodefoto;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            namalengkap = itemView.findViewById(R.id.textView_nama);
            pekerjaan = itemView.findViewById(R.id.textView_pekerjaan);
            fotoprofil = itemView.findViewById(R.id.gambaruser);

            itemView.setOnClickListener(this);
        }


        public void bindTo(Data currentUser) {
            namalengkap.setText(currentUser.getNama());
            pekerjaan.setText(currentUser.getPekerjaan());

            kodefoto = currentUser.getAvatar();
            switch ((currentUser.getAvatar())) {
                case 1:
                    fotoprofil.setImageResource(R.drawable.man);
                    break;
                case 2:
                default:
                    fotoprofil.setImageResource(R.drawable.woman);
                    break;
            }
        }

        @Override
        public void onClick(View v) {
            Intent toDetail = new Intent(v.getContext(), Detail.class);
            toDetail.putExtra("nama", namalengkap.getText().toString());
            toDetail.putExtra("gender", kodefoto);
            toDetail.putExtra("pekerjaan", pekerjaan.getText().toString());
            v.getContext().startActivity(toDetail);

        }
    }
}