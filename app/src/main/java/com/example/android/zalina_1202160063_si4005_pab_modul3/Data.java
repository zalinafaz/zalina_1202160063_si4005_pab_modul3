package com.example.android.zalina_1202160063_si4005_pab_modul3;

public class Data {
    private String nama;
    private String pekerjaan;

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public int getAvatar() {
        return avatar;
    }

    private final int avatar;

    public Data(String nama, String pekerjaan, int avatar) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.avatar = avatar;
    }
}
