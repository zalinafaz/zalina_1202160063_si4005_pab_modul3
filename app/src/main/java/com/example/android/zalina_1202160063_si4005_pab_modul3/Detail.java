package com.example.android.zalina_1202160063_si4005_pab_modul3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Detail extends AppCompatActivity {
    private TextView DetailNama, DetailPekerjaan;
    private ImageView DetailFoto;
    private int KodeFoto;
    private String mNama, mPekerjaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        DetailNama = findViewById(R.id.TextView_DetailNama);
        DetailPekerjaan = findViewById(R.id.TextView_DetailPekerjaan);
        DetailFoto = findViewById(R.id.ig_detail_Avatar);

        mNama = getIntent().getStringExtra("nama");
        mPekerjaan = getIntent().getStringExtra("pekerjaan");
        KodeFoto = getIntent().getIntExtra("gender", 2);

        DetailNama.setText(mNama);
        DetailPekerjaan.setText(mPekerjaan);
        switch (KodeFoto) {
            case 1:
                DetailFoto.setImageResource(R.drawable.man);
                break;
            case 2:
            default:
                DetailFoto.setImageResource(R.drawable.woman);
                break;
        }
    }
}
